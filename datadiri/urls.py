from django.urls import path
from .views import *

urlpatterns = [
    path('', homePageView, name='home'),
    path('profile/', profileView, name='profile'),
    path('interest/', interestView, name='interest'),
    path("waifu/", waifuView, name="waifu"),
]